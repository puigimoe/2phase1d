#!/usr/bin/env julia
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 11:05:55 2018

@author: chauchat
@author: bonamy (python to julia)
@cheatJULIA: https://cheatsheets.quantecon.org/
"""
###############################################################################
#
# Import librairies
#
###############################################################################
using PyPlot

###############################################################################
#
# Numerical parameters
#
###############################################################################
dt = 1e-3
tend = 30#*10000.0
kplot = 5/dt
#
h = 0.1
N = 51
dz = h/(float(N-1))

# Mesh definition
# scalar points
#zs = Array(0:dz:h)
zs = Array(range(0,stop=h,length=N))

zs = (1/h)*zs
# Velocity points
#zw = Array(-dz/2:dz:h+dz/2)
zw = Array(range(-dz/2,stop=h+dz/2,length=N+1))

zw = (1/h)*zw

zw[1]=0
zw[N+1]=1

# Internal nodes list
wNodes = range(1, stop=N-1)

scalarNodes = range(1, stop=N-2)
###############################################################################
#
# Physical parameters (Pham Van Bang et al. 2006)
#
###############################################################################
dp=290e-6
rhop=1200.0e0 # 1005e0

rhof = 995.0e0
nuf = 20e-3/rhof

g = 9.81e0

phiSmall=1e-6
phiMax=0.6

# Stokes response time and settling velocity
tp=rhop*dp^2/(18.0*nuf*rhof)
wstokes = (rhop-rhof)*g*dp^2/(18.0*rhof*nuf)
println("tpStokes = ",tp,"; Wstokes = ",wstokes)
# Richardson and Zaki exponent
RZ = 0 # -3

Wmax = 1.1

###############################################################################
##
## Declaration of variables
##
###############################################################################
# Volume fraction
phin = zeros(N)
phinp = zeros(N)
phiw = zeros(N+1)

# Pressure
pfnp = zeros(N)
pp   = zeros(N)

dpfdz = zeros(N+1)
dppdz = zeros(N+1)

# Velocities
wpn = zeros(N+1)
wfn = zeros(N+1)
wpst = zeros(N+1)
wfst = zeros(N+1)
wpnp = zeros(N+1)
wm = zeros(N+1)
wfnp = zeros(N+1)


# Matrices and RHS
# Volume fraction
Aa = zeros((N,N))
Ha = zeros(N)
# Velocities
Ap = zeros((N+1,N+1))
Af = zeros((N+1,N+1))
Apinv = zeros((N+1,N+1))
Am = zeros((N+1,N+1))
Afinv = zeros((N+1,N+1))
Ap_f = zeros((N+1,N+1))
Af_p = zeros((N+1,N+1))
Hp = zeros(N+1)
Hf = zeros(N+1)
# Pressure
Apf = zeros((N,N))
Hpf = zeros(N)

###############################################################################
#
# divergence & gradient operator in matrix form
#
###############################################################################
divv = zeros((N,N+1))
grad = zeros((N+1,N))

# gradient operator
for j in wNodes
    grad[j+1, j] = - 1/dz
    grad[j+1, j+1] =   1/dz
end
# Bottom boundary condition
j=1
grad[j, j  ] = -1/dz
grad[j, j+1] =  1/dz

# Top boundary condition
j=N+1
grad[j, j-2] = -1/dz
grad[j, j-1] =  1/dz

# Divergence operator
for j in wNodes
    divv[j+1, j+1] = - 1/dz
    divv[j+1, j+2] =   1/dz
end
# Bottom boundary condition
j=1
divv[j, j  ] = -1/(0.5*dz)
divv[j, j+1] =  1/(0.5*dz)
###############################################################################
#
# Initial condition
#
###############################################################################

phi0 = 0.5
h0 = 0.5*h
#phin = phi0*0.5*(1.0 .+ tanh.((h0/h .- zs)*300.0))
@. phin = phi0*0.5*(1.0 + tanh((h0/h - zs)*300.0))

wpn = fill(-wstokes/10.0, N+1)
wpn[1] = 0.0
# Interpolation of phi at W points
for j in scalarNodes
    phiw[j+1] = 0.5*(phin[j+1]+phin[j])
end
phiw[1] = phin[1]
phiw[N+1] = phin[N]

#wfn = - phiw .* wpn ./ (1.0 .- phiw)
@. wfn = - phiw * wpn / (1.0 - phiw)

sim_time = 0
k=0
###############################################################################
#
#  Temporal loop
#
###############################################################################
while (sim_time<tend)
    global sim_time, k, wpn, phin, Hp, wfn, Hf

    sim_time = sim_time + dt
    k = k + 1
    ########################################################
    #
    #        Mass conservation equation
    #
    ########################################################
    for j in scalarNodes
        FS = dt *  wpn[j+1] / dz
        FN = dt *  wpn[j+2] / dz
        #
        Aa[j+1, j] = - max( FS,0)
        Aa[j+1, j+2] = - max(-FN,0)
        Aa[j+1, j+1] = 1e0 -Aa[j+1, j+2] -Aa[j+1, j] + FN - FS
        Ha[j+1] = phin[j+1]
    end

    # Bottom boundary condition: Neumann
    j=1
    FN = dt *  wpn[j+1] / (0.5*dz)
    #
    Aa[j, j+1] = - max(-FN,0)
    Aa[j, j] = 1e0 -Aa[j, j+1] + FN
    Ha[j] = phin[j]

    # Top boundary condition: Dirichlet
    Aa[N, N]=1e0
    Ha[N]=0e0
    # Mass conservation resolution
    phinp = Aa \ Ha

        ########################################################
        #
        # Interpolation of phi at velocity points
        #
        ########################################################

        for j in wNodes
            phiw[j+1] = 0.5*(phin[j+1]+phin[j])
        end
        phiw[1] = phinp[1]
        phiw[N+1] = phinp[N]

        # Compute the particle pressure
        for j in range(1, stop=N)
            pp[j] = 5e-2 * ((max(phinp[j]-0.57,0.0))^3) / ((max(0.625-phinp[j],phiSmall))^5)
        end
        # Compute the vertical gradient of particle pressure
        dppdz = (grad*pp)

        ########################################################
        #
        # Velocity predictor
        #
        ########################################################
        #
        # Momentum conservation equations: Matrices for Wp and Wf
        for j in wNodes
            # Solid phase matrix
            if j==1
                FS = dt *  wpn[j] / dz
            else
                 FS = dt *  0.5*(wpn[j+1]+wpn[j]) / dz
            end
            if j==N-1
                FN = dt *  wpn[j+2] / dz
            else
                FN = dt *  0.5*(wpn[j+2]+wpn[j+1]) / dz
            end
            Ap[j+1,j] = - max( FS,0)
            Ap[j+1,j+2] = - max(-FN,0)
            Ap[j+1,j+1] = 1e0 - Ap[j+1,j] - Ap[j+1,j+2] + FN - FS + dt/tp*(1.0-phiw[j+1])^RZ
            Hp[j+1] = wpn[j+1] -dt*(1.0-rhof/rhop)*g -dt*dppdz[j+1]/(rhop*(phiw[j+1]+phiSmall))
            # -dt*(1.-rhof/rhop)*g

            # coupling Matrix for the solid phase
            Ap_f[j+1,j+1]= - dt/tp*(1.0-phiw[j+1])^RZ

            # fluid phase matrix
            if j==1
                FS = dt *  wfn[j] / dz
            else
                 FS = dt * 0.5*(wfn[j+1]+wfn[j]) / dz
            end
            if j==N-1
                FN = dt *  wfn[j+2] / dz
            else
                FN = dt *  0.5*(wfn[j+2]+wfn[j+1]) / dz
            end
            Af[j+1, j] = - max( FS,0)
            Af[j+1,j+2] = - max(-FN,0)
            Af[j+1,j+1] = 1e0 - Af[j+1,j] - Af[j+1,j+2] + FN - FS + dt*rhop*phiw[j+1]/(rhof*(1.0-phiw[j+1])*tp)*(1.0-phiw[j+1])^RZ
            Hf[j+1] = wfn[j+1] #- dt*g

            # coupling Matrix for the fluid phase
            Af_p[j+1,j+1] = - dt*rhop*phiw[j+1]/(rhof*(1.0-phiw[j+1])*tp)*(1.0-phiw[j+1])^RZ
        end
        # Bottom boundary condition: impermeable wall
        #Particle phase
        Ap[1,1]   = 1e0
        Ap_f[1,:] .= 0.0
        Hp[1]     = 0e0
        # Fluid phase
        Af[1,1]   = 1e0
        Af_p[1,:] .= 0.0
        Hf[1]     = 0e0
#        j=0
#        Ap[j,j  ] = -1
#        Ap[j,j+1] =  1
#        Af_p[j,:] = 0
#
#        Hp[j] = 0
#
#        Af[j,j  ] = -1
#        Af[j,j+1] =  1
#        Ap_f[j,:] = 0
#
#        Hf[j] = 0

        # Top boundary condition: momentum equation
        j=N+1
        Ap[j,j-1] = -1.0
        Ap[j,j  ] =  1.0
        Af_p[j,:] .= 0.0

        Hp[j] = 0.0

        Af[j,j-1] = -1.0
        Af[j,j  ] =  1.0
        Ap_f[j,:] .= 0.0

        Hf[j] = 0.0

        # Solid and fluid phase momentum conservation resolution
        Apinv = inv(Ap)
        Afinv = inv(Af)
        Hp = Hp - Ap_f*wfn
        wpst = Apinv*Hp

        Hf = Hf - Af_p*wpn
        wfst = Afinv*Hf

        # Compute mixture velocity and volume averaged matrices
#        wm = phiw .* wpst + (1e0 .- phiw) .* wfst
#        Am = phiw .* Apinv / rhop + (1e0 .- phiw) .* Afinv / rhof
         @. wm = phiw * wpst + (1e0 - phiw) * wfst
         @. Am = phiw * Apinv / rhop + (1e0 - phiw) * Afinv / rhof

        ########################################################
        #
        # Poisson pressure equation
        #
        ########################################################

        Apf = -dz*(divv*(Am*grad))
        Hpf = -dz*(divv*wm)/dt

        # Bottom boundary condition: zero gradient (not ideal)
        j = 1

        Apf[j,:] .= 0.0
        Apf[j,j+1] = 1.0/dz
        Apf[j,j  ] = -1.0/dz
        Hpf[j] =  wm[2]/(dt*Am[2,2])  #(phiw[1]*rhop+(1-phiw[1])*rhof)*g

        # Top boundary condition: Dirichlet pf=0
        j = N
        Apf[j,:] .= 0.0
        Apf[j,j] = 1.0
        Hpf[j] = 0.0

        # resolve poisson equation matrix
        pfnp = Apf \ Hpf

        # Compute fluid phase pressure gradient
        dpfdz = (grad*pfnp)

        ########################################################
        #
        # Velocity correction
        #
        ########################################################

        wpnp = wpst - dt/rhop*(Apinv*dpfdz)
        wfnp = wfst - dt/rhof*(Afinv*dpfdz)

        # Bottom boundary condition: impermeable wall
        wpnp[1]=0e0
        wfnp[1]=0e0
        wpnp[N+1]=wpnp[N]
        wfnp[N+1]=wfnp[N]

        ########################################################
        #
        # Check the resulting velocity fields are divergence free
        #
        ########################################################
#        wm = phiw .* wpnp + (1e0 .- phiw) .* wfnp
@.        wm = phiw * wpnp + (1e0 - phiw) * wfnp

        Continuity = (sum((divv*wm).^2)).^0.5/float(N)

        if (Continuity > 1e-4)
            println("Continuity error is too high:",Continuity)
        end

        ########################################################
        #
        #  Plot graphs at run time
        #
        ########################################################
        if (k==kplot)
            println("time=",sim_time, "max(phi)=",maximum(phinp))
            #plot
            figure(1,figsize=(12,8))
            subplot(141)
            plot(phinp,zs, color="red", linewidth=2.0, linestyle="-")
            #plot(phiw,zw,':k')
            axis([0, phiMax, 0, 1])
            #ylabel(r'$z/h$', fontsize=14)
            #xlabel(r'$\phi$', fontsize=12)

            subplot(142)
            plot(wpnp/wstokes,zw, color="red", linewidth=2.0, linestyle="--")
            #plot(wpst,z,':r')
            plot(wfnp/wstokes,zw, color="blue", linewidth=2.0, linestyle="--")
            #plot(wfst,z,':b')
            axis([-Wmax, Wmax, 0, 1])
            #xlabel(r'$\frac{W}{Wstokes}$',\
            #             fontsize=14)
            #yticks([])

            subplot(143)
            plot(pfnp/(phiMax*(rhop-rhof)*g*h0),zs, color="blue", linewidth=2.0, linestyle="--")
            plot(pp/(phiMax*(rhop-rhof)*g*h0),zs, color="red", linewidth=2.0, linestyle="--")
            axis([0, 1.1, 0, 1])
            #xlabel(r'$\frac{pf}{\phi_m (\rho^p-\rho^f) g h}$', \
            #             fontsize=14)
            #yticks([])


            subplot(144)
            plot(dpfdz/(phiMax*(rhop-rhof)*g),zw,"-b")
            plot(dppdz/(phiMax*(rhop-rhof)*g),zw,"--r")
            plot([-1,-1],[0, 1],":k")
            plot([-phi0/phiMax,-phi0/phiMax],[0, 1],":r")
            axis([-1.05, 0, 0, 1])
            #xlabel(r'$\frac{d pf/d z}{\phi_m (\rho^p-\rho^f) g}$ ', \
            #             fontsize=14)
            #yticks([])

            #show()
            savefig("test"*string(sim_time)*".png", format="png")
            close()
            # reset counter for updating plot
            k=0
        end
        ########################################################
        #
        #     Update variables for next time step
        #
        ########################################################
        wpn=wpnp
        wfn=wfnp
        phin=phinp






end
