#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 20:29:44 2018

@author: chauchat
"""

def compute_tp(N,dp,rhop,rhof,nuf,phi,dragModel):
    import numpy as np
    #
    #
    #
    tp = np.zeros(N+1)
    if (dragModel=='Darcy'):
        #
        # Darcy drag force
        #
        kd = 180.
        for j in range(N+1):
            K_d = (1.-phi[j])**3 * dp**2 / (kd * phi[j]**2)
            tp[j] = rhop * phi[j] * K_d / (rhof * nuf * (1.-phi[j])**2 ) 
            
    elif (dragModel=='Stokes'):
        #
        # Stokes drag force
        #    
        for j in range(N+1):
            tp[j] = rhop*dp**2/(18.*nuf*rhof)
        
    return tp

def compute_Wsettling(g,dp,rhop,rhof,nuf,phi,dragModel):
    #
    #
    #
    if (dragModel=='Darcy'):
        #
        # Darcy drag force
        #
        kd = 180.
        wsettling = (rhop-rhof)*g*dp**2/(18.*rhof*nuf)
 
            
    elif (dragModel=='Stokes'):
        #
        # Stokes drag force
        #    
        wsettling = (rhop-rhof)*g*dp**2/(18.*rhof*nuf)
        
    return wsettling