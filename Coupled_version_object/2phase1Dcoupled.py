#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 11:05:55 2018

@author: chauchat
"""
###############################################################################
#
# Import librairies
#
###############################################################################
import numpy as np
import scipy
import pylab as pylab
from MyClass import bedload
from plotting_function import plot
###############################################################################
#
# Parameters
#
###############################################################################
dt=1.e-4
tend=5#*10000
kplot=10000
#
h=0.1
h1 = 0.5*h
N=91
dz=h/(float(N-1))
#physical parameters
dp=4e-3
rhop=2500e0 # 1005e0
rhof = 1000e0
nuf = 500e-3/rhof
g = 9.81e0
RZ = -0

#Numerical parameters
alpha_ini = 0.5
phiSmall=1e-6
phiMax=0.6
Wmax = 1.1

#choose the configuration
config = "smallOnTop"

# Richardson and Zaki exponent

bedload = bedload(h, tend, dz, kplot, N, h1, alpha_ini, rhop, dp, rhof, nuf, g, dt, RZ, config)

#Initialisation
bedload.initialisation()
print('CFL=',bedload.wstokes*bedload.dt/bedload.dz,' dt/tp=',bedload.dt/bedload.tp)
bedload.addArgument()    
bedload.operator()

#bedload.interpolation(bedload.phiw, bedload.Iphi, bedload.phin)
bedload.phiw = bedload.Iphi.dot(bedload.phinp)

#calcul vitesse fluide            
bedload.wfn = - bedload.phiw * bedload.wpn / (1.-bedload.phiw)
    

###############################################################################
#
#  Temporal loop
#
###############################################################################
while (bedload.time<bedload.tend):
    
        bedload.time = bedload.time + bedload.dt
        bedload.k = bedload.k + 1
    
        #compute  continuity
        bedload.massConservation()

#        bedload.interpolation(bedload.phiw, bedload.Iphi, bedload.phinp)
#        bedload.interpolation(bedload.phiDrag, bedload.IphiDrag, bedload.phinp)
        bedload.phiw = bedload.Iphi.dot(bedload.phinp)
        bedload.phiDrag = bedload.IphiDrag.dot(bedload.phinp)
        
        
        #compute the solid pressure
        bedload.solidPressure()
        
        #compute the momentum equations
        bedload.advection_schemes()
        #######################################################################
        #######################################################################
        #building block matrix
        bedload.A = np.block([[bedload.Ap,bedload.Ap_f],[bedload.Af_p,bedload.Af]])
        bedload.H = np.block([bedload.Hp,bedload.Hf])
        
        bedload.Ainv = np.linalg.inv(bedload.A)
        bedload.wst = bedload.Ainv.dot(bedload.H)

        # Solid and fluid phase momentum conservation resolution
        bedload.Apinv = np.linalg.inv(bedload.Ap)
        bedload.Afinv = np.linalg.inv(bedload.Af)

        # Compute mixture velocity and volume averaged matrices
        Aphi = np.block([np.identity(N+1)*bedload.phiw,np.identity(N+1)*(1-bedload.phiw)])
        wm = Aphi.dot(bedload.wst)

        AoneOverRho = np.block([[np.identity(N+1)/bedload.rhop],\
                                [np.identity(N+1)/bedload.rhof]])
        Am = (Aphi.dot(bedload.Ainv)).dot(AoneOverRho)
        #######################################################################
        #######################################################################            
        
        #velocities correction
        bedload.poisson_correction(Am, wm, AoneOverRho)
        
        # Check the resulting velocity fields are divergence free 

        wm = Aphi.dot(bedload.wnp)
        Continuity = (np.sum(bedload.div.dot(wm)**2))**0.5/float(N)
        
        #if (Continuity > 1e-4):
        #    print('Continuity error is too high:',Continuity)

        ########################################################
        #
        #  Plot graphs at run time
        #
        ########################################################
        if (bedload.k==kplot):
            plot_results = plot(bedload.time, bedload.phinp, bedload.phiMax, bedload.zs, \
                                bedload.zw, bedload.wpnp, bedload.wfnp, bedload.rhop, \
                                bedload.rhof, bedload.g, bedload.h, bedload.alpha_ini, \
                                bedload.dpfdz, bedload.dppdz, bedload.pfnp, bedload.dp, \
                                bedload.wstokes, bedload.wmax, bedload.pp, bedload.h1)
            bedload.k = 0
        ########################################################
        #    
        #     Update variables for next time step
        #
        ########################################################
        bedload.wn = bedload.wnp
        bedload.wpn = bedload.wpnp
        bedload.wfn = bedload.wfnp
        bedload.phin = bedload.phinp
        