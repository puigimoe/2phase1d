import pylab as pylab
import numpy as np
import matplotlib.pyplot as plt

class plot(object):
    
    def __init__ (self, time, phinp, phiMax, zs, zw, wpnp, wfnp, rhop, rhof, g, h, alpha_ini, dpfdz, dppdz, 
                  pfnp, dp, wstokes, wmax, pp, h1):
        print("time=",time,'max(phi)=',np.max(phinp))
        #plot
        pylab.figure(1,figsize=(12,8))
        pylab.subplot(141)
        pylab.plot(phinp,zs,'-r')
        #pylab.plot(phiw,zw,':k')
        pylab.axis([0, phiMax*1.1, 0, 1])
        pylab.ylabel(r'$z/h$', fontsize=14)
        pylab.xlabel(r'$\phi$', fontsize=12)
         
        pylab.subplot(142)
        pylab.plot(wpnp/wstokes,zw,'-r')
        #pylab.plot(wpst,z,':r')
        pylab.plot(wfnp/wstokes,zw,'-b')
        #pylab.plot(wfst,z,':b')
        pylab.axis([-wmax, wmax, 0, 1])
        pylab.xlabel(r'$\frac{W}{Wstokes}$',\
                     fontsize=14)
        pylab.yticks([])
         
        pylab.subplot(143)
        pylab.plot(pfnp/(phiMax*(rhop-rhof)*g*h1),zs,'-b')
        pylab.plot(pp/(phiMax*(rhop-rhof)*g*h1),zs,'-r')
        pylab.axis([0, 1.1, 0, 1])
        pylab.xlabel(r'$\frac{pf}{\phi_m (\rho^p-\rho^f) g h}$', \
                     fontsize=14)
        pylab.yticks([])

         
        pylab.subplot(144)
        pylab.plot(dpfdz/(phiMax*(rhop-rhof)*g),zw,'-b')
        pylab.plot(dppdz/(phiMax*(rhop-rhof)*g),zw,'--r')
        pylab.plot([-1,-1],[0, 1],':k')
        pylab.plot([-alpha_ini/phiMax,-alpha_ini/phiMax],[0, 1],':r')
        pylab.axis([-1.05, 0, 0, 1])
        pylab.xlabel(r'$\frac{d pf/d z}{\phi_m (\rho^p-\rho^f) g}$ ', \
                     fontsize=14)
        pylab.yticks([])

        pylab.show()
